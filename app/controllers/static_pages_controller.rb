class StaticPagesController < ApplicationController

  def home
  end

  def about
  end

  def design
  end

  def projects
  end

  def contacts
  end

  def complete
    
  end

  def progress
    
  end

end
